using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// 세팅 패널을 나타내기 위한 컴포넌트
/// </summary>
public class SettingPanel : MonoBehaviour
{
    [Header("# 룰렛 게임 객체")]
    public RouletteGame m_RouletteGame;

    [Header("# 입력 필드")]
    public TMP_InputField[] m_InputFields;

    [Header("# 비우기 버튼")]
    public Button m_ClearButton;

    [Header("# 시작하기 버튼")]
    public Button m_StartButton;

    private void Awake()
    {
        // 버튼 이벤트 바인딩
        m_ClearButton.onClick.AddListener(OnClearButtonClicked);
        m_StartButton.onClick.AddListener(OnStartButtonClicked);
    }

    /// <summary>
    /// 비우기 버튼 클릭 시 호출되는 메서드입니다.
    /// </summary>
    private void OnClearButtonClicked()
    {
        foreach(TMP_InputField inputField in  m_InputFields)
            inputField.text = null;
    }

    /// <summary>
    /// 게임 시작 버튼 클릭 시 호출되는 메서드입니다.
    /// </summary>
    private void OnStartButtonClicked()
    {
        m_RouletteGame.m_RouletteObject.InitializeRoulette(GetInputFieldToArray());
        gameObject.SetActive(false);
    }

    /// <summary>
    /// 입력 필드의 내용을 배열로 반환합니다.
    /// </summary>
    /// <returns></returns>
    private string[] GetInputFieldToArray()
    {
        // 반환시킬 문자열(InputField 에 작성된 문자열)이 저장될 리스트
        List<string> itemStrings = new();

        foreach(TMP_InputField inputField in m_InputFields)
            itemStrings.Add(inputField.text);

        // 리스트의 내용을 배열로 변환하여 반환합니다.
        return itemStrings.ToArray();
    }

}
