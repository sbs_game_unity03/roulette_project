using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RouletteObject : MonoBehaviour
{
    [Header("# 룰렛 게임 객체")]
    public RouletteGame m_RouletteGame;

    [Header("# 룰렛 오브젝트")]
    public GameObject m_RouletteObject;

    [Header("# 룰렛에 표시되는 아이템 텍스트")]
    public List<TMP_Text> m_ItemTexts;

    [Header("# 룰렛 제동력")]
    public float m_BrakingForce = 1500.0f;

    [Header("# 결과 문자열")]
    public TMP_Text m_ResultText;

    [Header("# 재시작 버튼")]
    public Button m_RestartButton;

    /// <summary>
    /// 현재 적용된 z 축 회전 속도입니다.
    /// 0 일 경우 회전하지 않습니다.
    /// </summary>
    private float _ZRotationVelocity;

    /// <summary>
    /// 룰렛 오브젝트 회전 허용 여부입니다.
    /// </summary>
    private bool _IsRotatingAllowed;

    private void Start()
    {
        // 다시하기 버튼 비활성화
        m_RestartButton.gameObject.SetActive(false);

        // 다시하기 버튼 이벤트 바인딩
        m_RestartButton.onClick.AddListener(OnRestartButtonClicked);
    }

    private void Update()
    {
        // Input.GetKeyDown : 눌릴 경우 한번 True
        // Input.GetKey : 눌려있을 겨우 True
        // Input.GetKeyUp : 떼어졌을 경우 한번 True
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    InitializeRoulette();
        //}

        ShowResult();

        // 룰렛 회전
        RotationRoulette();
    }

    /// <summary>
    /// 룰렛 오브젝트를 초기화합니다.
    /// <paramref name="itemString"/>설정시킬 문자열들을 전달합니다.</param>
    /// </summary>
    public void InitializeRoulette(string[] itemString)
    {
        // 아이템 문자열을 모두 설정합니다.
        for (int i = 0; i < m_ItemTexts.Count; ++i)
        {
            // 텍스트 컴포넌트를 배열에서 얻습니다.
            m_ItemTexts[i].text = itemString[i];
        }

        // 회전 속도 초기화
        InitializeRotationVelocity();

        // 회전 허용
        _IsRotatingAllowed = true;
    }

    /// <summary>
    /// 회전 속도를 초기화합니다.
    /// </summary>
    private void InitializeRotationVelocity()
    {
        _ZRotationVelocity = Random.Range(2500.0f, 3500.0f);
    }

    /// <summary>
    /// 룰렛을 회전시킵니다.
    /// </summary>
    private void RotationRoulette()
    {
        if (_IsRotatingAllowed)
        {
            // 룰렛의 현재 회전을 얻습니다.
            Vector3 rouletteRotation = m_RouletteObject.transform.localEulerAngles;

            // 제동력을 적용합니다.
            _ZRotationVelocity -= m_BrakingForce * Time.deltaTime;

            // 회전 속도가 0 미만인 경우 회전을 진행하지 않습니다.
            if (_ZRotationVelocity < 0.0f)
            {
                // 다시하기 버튼 활성화
                m_RestartButton.gameObject.SetActive(true);

                ShowResult();

                // 회전을 멈추도록 합니다.
                _IsRotatingAllowed = false;
            }

            // 다음 회전값을 설정합니다.
            rouletteRotation.z -= _ZRotationVelocity * Time.deltaTime;

            // 룰렛의 회전을 설정합니다.
            m_RouletteObject.transform.localEulerAngles = rouletteRotation;
        }
    }

    /// <summary>
    /// 결과를 표시합니다.
    /// </summary>
    private void ShowResult()
    {
        // 핀과 가장 가까운 아이템을 나타내기 위한 변수
        TMP_Text nearestText = null;

        // 핀과 가장 가까운 아이템의 각도를 저장하기 위한 변수
        float nearestAngle = 360.0f;


        foreach (TMP_Text textComponent in m_ItemTexts)
        {
            float angle = Vector2.SignedAngle(textComponent.rectTransform.up, Vector2.up);
            angle = Mathf.Abs(angle);

            // 더 작은 각을 이루는 요소를 찾은 경우
            if (nearestAngle > angle)
            {
                nearestAngle = angle;
                nearestText = textComponent;
            }
        }

        // 결과를 표시합니다.
        m_ResultText.text = nearestText.text;
    }

    /// <summary>
    /// 다시하기 버튼 클릭 시 호출되는 메서드입니다.
    /// </summary>
    private void OnRestartButtonClicked()
    {
        // Setting Panel 활성화
        m_RouletteGame.m_SettingPanel.gameObject.SetActive(true);

        // 다시하기 버튼 비활성화
        m_RestartButton.gameObject.SetActive(false);
    }

    private void OnDrawGizmos()
    {
        RectTransform rouletteRectTransform = transform as RectTransform;

        // 그릴 때 사용될 좌표행렬 정보를 얻습니다.
        Matrix4x4 screenMatrix = (transform as RectTransform).localToWorldMatrix;

        // 기즈모가 그려질 좌표행렬 정보를 설정합니다.
        UnityEditor.Handles.matrix = Gizmos.matrix = screenMatrix;

        // 시작점을 설정합니다.
        Vector2 drawLineStart = rouletteRectTransform.anchoredPosition;

        foreach (TMP_Text textComponent in m_ItemTexts)
        {
            // 끝점을 설정합니다.
            Vector2 drawLineEnd = textComponent.rectTransform.up * 300.0f;

            // 룰렛의 중앙부터 핀까지의 선분과
            // 룰렛의 중앙부터 아이템 텍스트 위치까지의 선분 사이의 각도를 구합니다.
            float angle = Vector2.SignedAngle(textComponent.rectTransform.up, Vector2.up);
            /// Vector2.SignedAngle(from, to) : from 과 to 벡터 사이의 각도를 반환합니다.

            angle = Mathf.Abs(angle);

            // 선을 그립니다.
            Gizmos.DrawLine(drawLineStart, drawLineEnd);
            UnityEditor.Handles.Label(drawLineEnd, angle.ToString());

        }
    }
}
